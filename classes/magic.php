<?php

namespace classes;

class magic
{
    public int $id;
    public string $name;

    /**
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }


    public static function generateMagic(): array
    {
        $attackMagic = new \classes\magic(1, 'attackMagic');
        $defenceMagic = new \classes\magic(2, 'defenceMagic');
        return [$attackMagic, $defenceMagic];
    }

}