<?php

namespace classes;

class weapon
{
    public int $id;
    public string $name;
    public int $minDamage = 1;
    public int $maxDamage;
    public string $category;

    /**
     * @param int $id
     * @param string $name
     * @param int $minDamage
     * @param int $maxDamage
     */
    public function __construct(int $id, string $name, int $minDamage, int $maxDamage, string $category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->minDamage = $minDamage;
        $this->maxDamage = $maxDamage;
        $this->category = $category;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMinDamage(): int
    {
        return $this->minDamage;
    }

    public function setMinDamage(int $minDamage): void
    {
        $this->minDamage = $minDamage;
    }

    public function getMaxDamage(): int
    {
        return $this->maxDamage;
    }

    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setMaxDamage(int $maxDamage): void
    {
        $this->maxDamage = $maxDamage;
    }

    public static function generateWeapons(): array
    {
        $sword = new \classes\weapon(1, 'épée', 1, 8, "short");
        $dagger = new \classes\weapon(2, 'dague', 1, 4, "short");
        $staff = new \classes\weapon(3, 'bâton', 2, 6, "short");
        $axe = new \classes\weapon(4, 'hache', 1, 10, "short");
        $bow = new \classes\weapon(4, 'hache', 1, 5, "long");
        $sling = new \classes\weapon(4, 'hache', 1, 4, "long");
        return [$sword, $dagger, $staff, $axe, $bow, $sling];
    }

}